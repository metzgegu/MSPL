---
title: "Projet Statistique"
author: "Thomas Guevara - Guillaume Metzger"
date: "3 avril 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
\begin{center}
\includegraphics{StatImage.png}
\end{center}

\newpage
# Tables des mati�res
- Probl�matiques
- Introduction
  - Table1
  - Table2
- Pr�paration des donn�es
- Quel sont les d�partements Fran�ais les moins scolaris�?
  - Conclusion: Departement
- Est ce que le genre � une importance dans les �tudes?
  - Conclusion: Genre
- A partir de quelle classe d'�ge les �tudes sont-elles abandonn�? 
  - Conclusion: Classe d'�ge

# Probl�matiques
- Quel sont les d�partements Fran�ais les moins scolaris�?
- Est ce que le genre � une importance dans les �tudes?
- A partir de quelle classe d'�ge les �tudes sont-elles abandonn�? 

# Introduction
Nous avons r�cup�r� sur le site web INSEE des bases de donn�es contenant les informations sur le niveau d'�tudes, les classes d'�ge et le genre r�pertori� en France en 2014. Ses donn�es vont nous permettre de r�pondre aux probl�matiques vues plus haut. Nous allons cr�er plusieurs graphiques qui vont nous permettre de conclure. De plus nous allons vous expliquez plus bas les diff�rentes variables des bases de donn�es

## Information sur l'IRIS
Dans un premier temps nous allons expliqu� ce que l'IRIS signifie, et son histoire. l'INSEE avait d�velopp� un d�coupage du territoire en mailles de taille homog�ne appel�es IRIS2000. Un sigle qui signifiait � Ilots Regroup�s pour l'Information Statistique � et qui faisait r�f�rence � la taille vis�e de 2 000 habitants par maille �l�mentaire.

\newpage

# Information sur les jeux de donn�es
Nous poss�dons plusieur variables qui vont nous permettre de r�pondre aux questions que l'on c'est pos�, de plus nousvons dus faire une recherche d'information compl�mentaire, donc nous utilisons une autre sources de donn�es que celle de base. a

##Table 1
- DEP: Le num�ro du d�partement Fran�ais
- P14_POP0205 : Population 2 � 5 ans en 2014
- P14_POP0610 : Population 6 � 10 ans en 2014
- P14_POP1114 : Population 11 � 14 ans en 2014
- P14_POP1517 : Population 15 � 17 ans en 2014 
- P14_POP1824 : Population 18 � 24 ans en 2014 
- P14_POP2529 : Population 25 � 29 ans en 2014 
- P14_POP30P : Population 30 ans ou plus ans en 2014
- P14_SCOL0205 : Population scolaris�e 2 � 5 ans en 2014
- P14_SCOL0610 : Population scolaris�e 6 � 10 ans en 2014
- P14_SCOL1114 : Population scolaris�e 11 � 14 ans en 2014
- P14_SCOL1517 : Population scolaris�e 15 � 17 ans en 2014
- P14_SCOL1824 : Population scolaris�e 18 � 24 ans en 2014 
- P14_SCOL2529 : Population scolaris�e 25 � 29 ans en 2014 
- P14_SCOL30P : Population scolaris�e 30 ans ou plus en 2014
- P14_NSCOL15P : Population 15 ans ou plus non soclaris�e en 2014
- P14_NSCOL15P_DIPLMIN : Population 15 ans ou plus non scolaris�e Sans dipl�me ou BEPC, brevet des coll�ge, DNB en 2014
- P14_NSCOL15P_CAPBEP : Population 15 ans ou plus non scol, CAP-BEP en 2014
- P14_NSCOL15P_BAC : Population 15 ans ou plus non soclaris�, BAC en 2014
- P14_NSCOL15P_SUP : Populatiuon 15 ans ou plus non scolaris�e, ENseignement sup�rieur en 2014
- P14_HNSCOL15P : Hommes 15 ans ou plus non scolaris�s en 2014
- P14_HNSCOL15P_DIPLMIN : Hommes 15 ans ou plus non scolaris�e, Sans dipl�me ou BEPC, brevet des coll�ges, DNB en 2014
- P14_HNSCOL15P_CAPBEP : Hommes 15 ans ou plus non scolaris�e CAP-BEP en 2014
- P14_HNSCOL15P_BAC : Hommes 15 ans ou plus non scol BAC en 2014
- P14_HNSCOL15P_SUP : Hommes 15 ans ou plus non scol Enseigement sup en 2014
- P14_FNSCOL15P : Femmes 15 ans ou plus non scolaris�es en 2014
- P14_FNSCOL15P_DIPLMIN : Femmes 15 ans ou plus non scol Sans dipl�me ou BEPC, brevet des coll�ges, DNB en 2014
- P14_FNSCOL15P_CAPBEP : Femmes 15 ans ou plus non scol CAP-BEP en 2014
- P14_FNSCOL15P_BAC : Femmes 15 ans ou plus non scolaris�e BAC en 2014
- P14_FNSCOL15P_SUP : Femmes 15 ans ou plus non scol, Enseignement sup�rieur en 2014

## Table 2
- P14_POPH : Populations d'Hommes en 2014
- P14_POPF : Populations de Femmes en 2014

```{r echo=FALSE, warning=FALSE, message=FALSE}
library(readr)
library(gdata)
library('sp')
library('cartography')
library(ggplot2)
library('cowplot')
```

# Pr�paration des donn�es
```{r }
table = read.csv("IRIS.csv",header=TRUE,sep=";", skip=5)
table2 = read.csv("base-ic-evol-struct-pop-2014.csv",header=TRUE,sep=";", skip=5)
table$DEP <- as.numeric(table$DEP)
table2$DEP <- as.numeric(table2$DEP)
newtable<-aggregate(table[,13:ncol(table)], by=list(Departement=table$DEP), FUN=sum)
newtable2<-aggregate(table2[,13:ncol(table2)], by=list(Departement=table2$DEP), FUN=sum)
SupDep<-c(96,97,98,99,100)
newtable<-newtable[-SupDep,]
newtable2<-newtable2[-SupDep,]

Corse<-subset(newtable,Departement==20)
Corse$Departement=as.character('2A')
tableF<-rbind(newtable,Corse)
tableF["20","Departement"]<-as.character("2B")
tableF<-tableF[order(tableF$Departement),]

Corse<-subset(newtable2,Departement==20)
Corse$Departement=as.character('2A')
tableF2<-rbind(newtable2,Corse)
tableF2["20","Departement"]<-as.character("2B")
tableF2<-tableF2[order(tableF$Departement),]

SommeDep=tableF$P14_POP0205+tableF$P14_POP0610+tableF$P14_POP1114+tableF$P14_POP1517+
  tableF$P14_POP1824+tableF$P14_POP2529+tableF$P14_POP30P
tableFTot<-cbind(tableF,SommeDep)

load(url("http://wukan.ums-riate.fr/r2016/data/Occitanie.RData"))
```

# Quel sont les d�partements Fran�ais les moins scolaris�?
Nous cr�eons une carte de la France contenant les effectifs des personnes n'�tant pas scolaris� apr�s 15 ans.

```{r echo=FALSE, message=FALSE}
list_dep = gsub("^0", "", dep$CODE_DEPT)

tableF$Departement[!is.element(tableF$Departement, list_dep)]
list_dep[!is.element(list_dep, tableF$Departement)]

rbPal <- colorRampPalette(c('green', 'yellow', 'orange', 'red'))

qt = quantile(tableF$P14_NSCOL15P, c(0.25, 0.5, 0.75, 1))
leg = c(min(tableF$P14_NSCOL15P)-1, qt)
col <- rbPal(length(qt))[as.numeric(cut(tableF$P14_NSCOL15P, breaks = leg))] 

pos = as.vector(unlist(sapply(list_dep, function(c) which(c==as.character(tableF$Departement)))))

plot(dep, col=  col[pos])
legend("right", fill=rbPal(length(qt)), legend=paste(leg[-length(leg)], "-", leg[-1]),cex = 0.7)
```
Les d�partements en vert sont ceux qui poss�de le moins d'effectif de personne de plus de 15 ans qui ne sont pas scolaris� � l'oppos� nous avons les d�partements en rouge qui repr�sente ceux qui ont le plus d'effectif de personne non scolaris�. Cependant, nous comptons parmi eux les travailleurs soient ceux ayant plus de 30 ans � quelque exception pr�s.

## Conclusion : D�partement
Nous ne pouvons donc pas en tir� de conclusion tr�s pertinent, en effet il nous manque certaines donn�es pour pouvoir vraiment conclure par une affirmation sur et logique. De plus on observe que c'est en g�n�ral des d�partements comportant les plus grandes villes, comme Paris, Bordeaux, Alsace ... Ce qui peux expliquer la croissance importante de la population de personne n'�tant pas scolaris�.

 
# Est ce que le genre � une importance dans les �tudes?

```{r echo=FALSE}
ValHP<-sum(tableF$P14_HNSCOL15P)
ValHBrevet<-sum(tableF$P14_HNSCOL15P_DIPLMIN)
ValHCAP<-sum(tableF$P14_HNSCOL15P_CAPBEP)
ValHBAC<-sum(tableF$P14_HNSCOL15P_BAC)
ValHSUP<-sum(tableF$P14_HNSCOL15P_SUP)

ValFP<-sum(tableF$P14_FNSCOL15P)
ValFBrevet<-sum(tableF$P14_FNSCOL15P_DIPLMIN)
ValFCAP<-sum(tableF$P14_FNSCOL15P_CAPBEP)
ValFBAC<-sum(tableF$P14_FNSCOL15P_BAC)
ValFSUP<-sum(tableF$P14_FNSCOL15P_SUP)

tab.croise<-data.frame(Homme=c(ValHP,ValHBrevet,ValHCAP,ValHBAC,ValHSUP),
                       Femme=c(ValFP,ValFBrevet,ValFCAP,ValFBAC,ValFSUP),
                       row.names=c("Non scol","Sans dipl�me","CAP-BEP","BAC"," Enseignement sup"))
tab.croise<-t(data.matrix(tab.croise, rownames.force = NA))

barplot(tab.croise,beside =TRUE,legend.text = TRUE,col=c("chartreuse","darkorchid1"),ylim=c(0,25000000),
        main="R�partition des �tudes selon le sexe",xlab="Niveau d'�tudes",ylab="Effectifs")
```
Nous avons au d�part la repr�sentation totale des personnes de plus de 15 ans n'�tant pas scolaris� selon l'homme et la femme. On observe qu'il y a plus de femme non scolaris�e que d'hommes. Ensuite on peut observer que les femmes sont plus nombreuses � ne pas avoir de brevet, de bac ou d'�tudes sup�rieur. Mais pour ce qui est du CAP ce sont les hommes qui sont plus nombreux � ne pas avoir.
Si la population fran�aise est constitu�e de plus de femmes que d'hommes �a expliquerais pourquoi elles sont plus nombreuses dans les colonnes. En effet, �a suivrait une logique, donc nous allons observer le nombre de femme et d'hommes n'�tant pas scolaris� sur la population totale.

```{r, echo=FALSE}
TotHomme<-sum(tableF2$P14_POPH)
TotFemme<-sum(tableF2$P14_POPF)
Tot<-TotHomme+TotFemme

PercentH<-round((TotHomme/Tot)*100,digits=2)
PercentF<-round((TotFemme/Tot)*100,digits=2)

pie.HF=pie(c(PercentH,PercentF),label=c(PercentH,PercentF),col=c("chartreuse","darkorchid1"), 
            main="Diagramme circulaire de la population Homme/Femme")
legend("bottomright",legend=c("Homme","Femme"), fill = c("chartreuse","darkorchid1"),cex=0.7)
```

Dans un premier temps nous avons fait un graphe de la population total de femme et d'homme en France en 2014. Comme nous pouvions nous attendre il y a plus de femme que d'homme en France en 2014. Ce qui peut �tre un indicateur de la raison du nombre sup�rieur de femme non scolaris�e que l'on a observ� auparavant. Nous allons donc comparer le pourcentage de femme non scolaris� � celui des hommes ainsi nous obtiendrons l'information sur la question de l'importance des genres sur les �tudes en France.

```{r, echo=FALSE}
PercentHTH<-round((ValHP/TotHomme)*100,digits=2)
PercentFTF<-round((ValFP/TotFemme)*100,digits = 2)
PercentHBTH<-round((ValHBrevet/TotHomme)*100,digits=2)
PercentFBTF<-round((ValFBrevet/TotFemme)*100,digits = 2)
PercentHCAPTH<-round((ValHCAP/TotHomme)*100,digits=2)
PercentFCAPTF<-round((ValFCAP/TotFemme)*100,digits = 2)
PercentHBACTH<-round((ValHBAC/TotHomme)*100,digits=2)
PercentFBACTF<-round((ValFBAC/TotFemme)*100,digits = 2)
PercentHSUPTH<-round((ValHSUP/TotHomme)*100,digits=2)
PercentFSUPTF<-round((ValFSUP/TotFemme)*100,digits = 2)

tab.HF<-data.frame(Ordre=c(1,2),Nom=c("Homme","Femme"),Resultat=c(PercentHTH,PercentFTF))
tab.HF1<-data.frame(Ordre=c(1,2),Nom=c("Homme","Femme"),Resultat=c(PercentHBTH,PercentFBTF))
tab.HF2<-data.frame(Ordre=c(1,2),Nom=c("Homme","Femme"),Resultat=c(PercentHBACTH,PercentFBACTF))
tab.HF3<-data.frame(Ordre=c(1,2),Nom=c("Homme","Femme"),Resultat=c(PercentHCAPTH,PercentFCAPTF))
tab.HF4<-data.frame(Ordre=c(1,2),Nom=c("Homme","Femme"),Resultat=c(PercentHSUPTH,PercentFSUPTF))

t1<-ggplot(tab.HF,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Genre", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des hommes et \n des femmes qui ne sont pas scolaris�s")+
  geom_col(fill = c("chartreuse","darkorchid1"))+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
t2<-ggplot(tab.HF1,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Genre", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des hommes et \n des femmes qui ont un brevet")+
  geom_col(fill = c("chartreuse","darkorchid1"))+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
t3<-ggplot(tab.HF2,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Genre", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des hommes et \n des femmes qui ont un Bac")+
  geom_col(fill = c("chartreuse","darkorchid1"))+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
t4<-ggplot(tab.HF3,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Genre", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des hommes et \n des femmes qui ont un CAP")+
  geom_col(fill = c("chartreuse","darkorchid1"))+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
t5<-ggplot(tab.HF4,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Genre", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des hommes \n des femmes qui ont des �tudes sup�rieur")+
  geom_col(fill = c("chartreuse","darkorchid1"))+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
plot_grid(t1, t2, t3, t4, t5, align = "v", nrow = 3, ncol=2)
```

Nous observons une l�g�re sup�riorit� sur le pourcentage de femme qui ne sont pas dans les �tudes, en effet il n'y a que 2.13% de femme en plus qui ne sont pas scolaris� par rapport aux hommes. Nous observons qu'en g�n�rale les probabilit�s sont sup�rieur pour les femmes que les hommes sauf pour le CAP ou c'est les hommes qui sont nettement sup�rieur. Donc on peut en conclure que les femmes sont en g�n�rale moins dipl�m� que les hommes. Mais si nous observons les donn�es d'il y a 30 ans nous pourrions observer une �norme diff�rence car les femmes ne commencent que depuis peu � poursuivre leur �tude.

## Conclusion : Sur les genres
Il nous manque des informations compl�mentaires pour pouvoir tirer des conclusions d�finitives, en effet nos donn�es nous fournisse les informations sur le nombre de femme et d'homme �g�s de plus de 15 ans qui ne sont pas scolaris� ou leur niveau d'�tudes � la date de 2014. Nous pourrions avec des donn�es en plus regarder l'�volution dans le temps. Nous pouvons en conclure que quel que soit le genre nous arrivons pratiquement � une moyenne entre les deux dans le niveau d'�tude. 

\newpage
# A partir de quelle classe d'�ge les �tudes sont-elles abandonn� ? 
Nous poss�dons 7 classe d'�ge, pour chacune d'elle nous allons calculer le pourcentage de chacune d'entre elle

```{r echo=FALSE}
PopSco0205<-sum(tableF$P14_SCOL0205)
PopSco0610<-sum(tableF$P14_SCOL0610)
PopSco1114<-sum(tableF$P14_SCOL1114)
PopSco1517<-sum(tableF$P14_SCOL1517)
PopSco1824<-sum(tableF$P14_SCOL1824)
PopSco2529<-sum(tableF$P14_SCOL2529)
PopSco30<-sum(tableF$P14_SCOL30P)

DiaCir<-data.frame(Age2a5=c(PopSco0205),Age6a10=c(PopSco0610),Age11a14=c(PopSco1114),
                   Age15a17=c(PopSco1517),Age18a24=c(PopSco1824),Age25a29=c(PopSco2529),
                   Age30plus=c(PopSco30))
DiaCir<-round(prop.table(DiaCir)*100,digits = 2)
DiaCir<-data.matrix(DiaCir, rownames.force = NA)
pielabels<-c("Ag� de 2 � 5 ans","Ag� de 6 � 10 ans","Ag� de 11 � 14 ans",
             "Ag� de 15 � 17 ans","Ag� de 18 � 24 ans","Ag� de 25 � 29 ans",
             "Ag� de 30 ans et plus")

pie(DiaCir,label=c(DiaCir[1],DiaCir[2],DiaCir[3],DiaCir[4],DiaCir[5],
                   DiaCir[6],DiaCir[7]),
    col=c("blue","red","green","pink","purple","yellow","orange"), 
    main="Diagramme circulaire des pourcentages des classes d'age")
legend("bottomright",legend=pielabels, fill = c("blue","red","green","pink",
                                                "purple","yellow","orange"),cex=0.7)
```

Nous pouvons faire un constat flagrant � l'aide de ce graphique, en effet apr�s 24 ans le nombre de personne qui poursuivent les �tudes diminue drastiquement. Dans cette classe il ne doit y rester que les �tudiant en longue �tudes (en m�decine) ou les personnes qui se r�oriente apr�s les 30 ans. La classe d'�ge o� nous observons le plus de personne sont dans la classe d'�ge de 6 � 10 ans. Cependant nous devons prendre en compte le fait que la population fran�aise est compos�e de plus de jeunes entre 6 � 10 ans ce qui expliquerais le nombre sup�rieur dans ces �tudes. Nous devons donc voir le taux de population de chaque classe d'�ge.

```{r echo=FALSE}
Pop0205<-sum(tableF$P14_POP0205)
Pop0610<-sum(tableF$P14_POP0610)
Pop1114<-sum(tableF$P14_POP1114)
Pop1517<-sum(tableF$P14_POP1517)
Pop1824<-sum(tableF$P14_POP1824)
Pop2529<-sum(tableF$P14_POP2529)
Pop30<-sum(tableF$P14_POP30P)
SommePop<-Pop0205+Pop0610+Pop1114+Pop1517+Pop1824+Pop2529+Pop30

DiaCir2<-data.frame(Age2a5=c(Pop0205),Age6a10=c(Pop0610),Age11a14=c(Pop1114),
                    Age15a17=c(Pop1517),Age18a24=c(Pop1824),Age25a29=c(Pop2529),
                    Age30plus=c(Pop30))
DiaCir2<-round(prop.table(DiaCir2)*100,digits = 2)
DiaCir2<-data.matrix(DiaCir2, rownames.force = NA)

pie(DiaCir2,label=c(DiaCir2[1],DiaCir2[2],DiaCir2[3],DiaCir2[4],
                    DiaCir2[5],DiaCir2[6],DiaCir2[7]),
    col=c("blue","red","green","pink","purple","yellow","orange"), 
    main="Diagramme circulaire des pourcentages des classes d'age")
legend("bottomright",legend=pielabels, fill =c("blue","red","green","pink",
                                               "purple","yellow","orange"),cex=0.7)
```

Nous allons ne pas prendre en compte la classe d'�ge de 30 ans et plus car comme on peut l'envisager la majorit� des gens ont fini leur �tude et sont des travailleurs a pleins temps on a seulement quelque exception. Contrairement ce � quoi on pourrait s'attendre c'est la classe d'�ge des "18 � 24 ans" qui poss�de le plus de population par rapport au "6 � 10 ans" donc on ne peut pas dire que c'est parce qu'il y a plus de jeune de cette classe qui provoque le pourcentage �lev� vue dans le graphique pr�c�dent. Pour finaliser l'�tudes de cette information et de r�pondre � la question : "A partir de quelle classe d'�ge les �tudes sont-elles abandonn� ? " nous allons regarder la proportion de personne de chaque classe.

```{r echo=FALSE}
Res0205<-(PopSco0205/Pop0205)
Res0610<-(PopSco0610/Pop0610)
Res1114<-(PopSco1114/Pop1114)
Res1517<-(PopSco1517/Pop1517)
Res1824<-(PopSco1824/Pop1824)
Res2529<-(PopSco2529/Pop2529)
Res30<-(PopSco30/Pop30)

tab.CompClass<-data.frame(Ordre=c(1,2,3,4,5,6,7),
                          Nom=c("Age2a5","Age6a10","Age11a14","Age15a17",
                                "Age18a24","Age25a29","Age30plus"),
                          Resultat=c(Res0205,Res0610,Res1114,Res1517,
                                     Res1824,Res2529,Res30))
tab.CompClass$Resultat<-round(tab.CompClass$Resultat*100,digits = 2)

ggplot(tab.CompClass,aes(x=reorder(Nom,Ordre),y=Resultat))+
  geom_bar(stat="identity")+
  labs(x="Classe d age", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des pourcentage de personne scolaris� 
          \n pour chaque classe d'age")+
  geom_text(aes(label=Resultat), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
```

Ici nous avons fait la proportion de personne scolaris� par rapport au totale de personne appartenant � la classe. Ainsi on peut observer les pourcentages de personnes scolaris� de chaque classe.
Nous constatons que les classes d'�ges le plus proches des 100% sont les classes entre 6 � 14 ans, et cela semble logique sachant que jusqu'� 16 ans l'�cole est obligatoire. Cependant entre 2 � 5 ans il y a de plus en plus de m�re pr�f�re mettre en place l'instruction � domicile (anglais homeschooling) pour leur enfant ce qui explique que l'on ait que 74.11% de personne scolaris�. Entre 15 et 17 ans on a une l�g�re baisse. Nous observons qu'entre 18 et 24 ans nous avons plus que 52,23% des personnes qui continue leur �tude, sois la plupart s'arr�te apr�s un bac, bac pro, DUT, BTS etc... Entre 25 et 29 ans ce sont les personnes qui font des longues �tudes comme les m�decins, les �tudiants en droits... Ce qui explique que l'on ne poss�de plus que 7.7% de personne scolaris�. Passer les 30 ans il ne reste plus que les reconversions ou les personnes en poursuite d'�tudes longue qui ont redoubl�s.


```{r echo=FALSE}
SommeDepNS=(tableFTot$P14_POP0205+tableFTot$P14_POP0610+tableFTot$P14_POP1114)/SommeDep
SommeDepNS<-SommeDepNS*100
MeanDepM15=mean(SommeDepNS)
MeanDepP15=100-MeanDepM15

SommeDepNS=tableFTot$P14_NSCOL15P/(tableFTot$P14_POP1517+tableFTot$P14_POP1824+tableFTot$P14_POP2529+tableFTot$P14_POP30P)
SommeDepNS<-SommeDepNS*MeanDepP15
MeanDepNSP15=mean(SommeDepNS)
MeanDepNSM15=MeanDepP15-MeanDepNSP15
MeanDepM15<-round(MeanDepM15,digits = 2)
MeanDepP15<-round(MeanDepP15,digits = 2)
MeanDepNSP15<-round(MeanDepNSP15,digits = 2)
MeanDepNSM15<-round(MeanDepNSM15,digits = 2)

tabres <- data.frame(Ordre=c(1,2,3,4),Ens=c("Totale","Totale","15 ans et plus","15 ans et plus"), Moyenne= c(MeanDepM15,MeanDepP15,MeanDepNSP15,MeanDepNSM15),Intitule=c("moins de 15 ans","15 ans et plus","Non Scolaris�","Scolaris�"))

ggplot(data=tabres, aes(x=reorder(Ens,Ordre), y=Moyenne, fill=Intitule)) +
  geom_bar(stat="identity")+
  labs(x="Donn�e", y="Pourcentage")+
  ggtitle(label="Diagramme en barre des pourcentage de personne non scolaris� \n pour les 15 ans et plus")+
  geom_text(aes(label=Moyenne), vjust=1.6, color="white", size=3.5)+
  theme_minimal()
```

Nous avons � droite la population totale en France avec le pourcentage de personne �g� de 15 ans ou plus ainsi que le pourcentage de personne �g� de moins de 15 ans. A droite nous avons le pourcentage de personne �g� de plus de 15 ans, scolaris� et non scolaris�. Nous avons donc seulement 8% de la population de plus de 15 ans qui est scolaris� en 2014, et 76.14% qui sont d�j� dipl�m� ou qui ont abandonn� les �tudes.

## Conclusion : Classe d'Age
En France la plus gros partis de la population ont plus de 30 ans et ne sont pas ou peu scolaris�. Les classes d'�ge ou on observe le plus de personne scolaris�e sont entre 6 � 14 ans cependant ce n'est pas l� ou est concentr� le plus de population. 
Nous pouvons donc conclure en disant que l'�tat respecte les lois qui oblige les jeunes � aller � l'�cole jusqu'� 16 ans, et que c'est bien pour �a que c'est durant ces p�riodes ou on a le plus de personnes de la classe d'�ge qui sont scolaris�.
